# Proxy Reverso
Boilerplate para proxy reverso com nginx com letsencrypt

## Uso
Cada aplicação deve usar um nginx como o exemplo:
```yaml
version: '3'
services:
    web:
        image: nginx
        container_name: web
        environment:
          VIRTUAL_HOST: web.example.com
          LETSENCRYPT_HOST: web.example.com
    
    php-fpm:
        image: php-fpm
        container_name: php-fpm

networks:
  reverse-proxy:
    external: true
```
> Definindo o VIRTUAL_HOST para se tornar acessível e LETSENCRYPT_HOST para que os certificados sejam gerados e renovados 

## Como configurar o servidor de produção e testes

1. Crie um droplet na digital ocean com ubuntu

1. Instale o docker seguindo esse [tutorial](https://www.digitalocean.com/community/tutorials/como-instalar-e-usar-o-docker-no-ubuntu-18-04-pt)

1. Instale o gitlab-runner seguindo a [documentação](https://docs.gitlab.com/runner/install/linux-manually.html)
   * Instalação: 
   ```shell script
     sudo curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64
    ```
   * Adicione o usuário gitlab-runner ao grupo `docker`
   ```shell script
     sudo usermod -aG docker gitlab-runner
    ```
   
1. Crie um subgrupo para o projeto no gitlab

1. Registre um gitlab-runner para o subgrupo
    * Troque o `<TOKEN>`
    ```shell script
    sudo gitlab-runner register \
      --non-interactive \
      --url "https://gitlab.com/" \
      --registration-token "<TOKEN>" \
      --executor "docker" \
      --docker-image tmaier/docker-compose:latest \
      --docker-volumes /var/run/docker.sock:/var/run/docker.sock \
      --description "docker-runner" \
      --tag-list "docker"
    ```
1. Inicie o swarm mode
    * Altere `<IP>` para o ip do droplet 
    ```shell script
    sudo docker swarm init --advertise-addr <IP> 
    ```

1. Utilize o runner registrado para implantar o docker-compose desse repositório com proxy-reverso e let's encrypt.

1. Utilize a rede `reverse-proxy` nos stack que serão implantados:
   * docker-compose.yml 
   ```shell script
    networks:
      reverse-proxy:
        external: true
    
    ```
   * service nginx
   ```shell script
    networks:
        - reverse-proxy
        - default
    ```
